package weka.filters.unsupervised.instance;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.OptionMetadata;
import weka.filters.SimpleBatchFilter;

import java.util.ArrayList;

public abstract class InjectingFilter extends SimpleBatchFilter {
    @Override
    protected Instances determineOutputFormat(Instances inputFormat) throws Exception {
        int numAttributes = inputFormat.numAttributes();
        int classIndex = inputFormat.classIndex();
        for (Instance instance : inputFormat) {
            if (instance.numAttributes() != numAttributes) {
                throw new IllegalArgumentException(String.format(
                        "Instances are required to have the same number of attributes. " +
                                "Expected %d, got %d.\nInstance: %s",
                        numAttributes, instance.numAttributes(), instance.toString()
                ));
            }
            if (instance.classIndex() != classIndex) {
                throw new IllegalArgumentException(String.format(
                        "Instances are required to have the class attribute defined at the " +
                                "same index. " +
                                "Expected %d, got %d.\nInstance: %s",
                        classIndex, instance.classIndex(), instance.toString()
                ));
            }
        }

        /* The output has the same format as input. So just copy the attributes. */
        ArrayList<Attribute> atts = new ArrayList<>();
        for (int i = 0; i < inputFormat.numAttributes(); i++) {
            atts.add(inputFormat.attribute(i));
        }
        Instances output = new Instances("features", atts, 0);
        output.setClassIndex(inputFormat.classIndex());
        return output;
    }

    protected int m_runTag;
    @OptionMetadata(
            displayName = "Run tag",
            description = "Has no effects but just to differentiate multiple runs in the database.",
            displayOrder = 100,
            commandLineParamName = "runTag",
            commandLineParamSynopsis = "-runTag"
    )
    public int getRunTag() {
        return m_runTag;
    }
    public void setRunTag(int runTag) {
        this.m_runTag = runTag;
    }
}
