package weka.filters.unsupervised.instance;

import weka.core.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

public class Smear extends InjectingFilter implements Randomizable {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(Mixup.class.getName());

    /**
     * The seed used in randomization
     */
    protected int m_seed;

    /**
     * Determine how many samples to generate per instance.
     */
    protected int m_numSamples = 2;

    /**
     * The standard deviation of the Gaussian noise to add.
     */
    protected double m_stdDev = 1;

    /**
     * The random generator.
     */
    protected Random m_random;

    /* Initializer block */
    {
        m_seed = 1;
        m_random = new Random(m_seed);
    }

    @Override
    public void setSeed(int seed) {
        m_seed = seed;
        m_random = new Random(seed);
    }

    @Override
    public int getSeed() {
        return m_seed;
    }

    @Override
    public String globalInfo() {
        return null;
    }

    @Override
    protected Instances process(Instances instances) throws Exception {
        if (!isFirstBatchDone()) {
            int numOutputInstances = m_numSamples * instances.numInstances();

            /* Determine the scale of noise for each attribute. */
            final int SMALLEST_GAPS_TO_MEASURE = 10;
            double[] scales = kthSmallestGapsOfInstances(instances, SMALLEST_GAPS_TO_MEASURE);

            Instances output = getOutputFormat();

            int index = 0;
            while (output.numInstances() < numOutputInstances) {
                /* Simply sequentially iterate in the entire dataset instead of Bagging */
                Instance instance = instances.get(index);
                int numAttributes = instance.numAttributes();
                double[] attrs = instance.toDoubleArray();
                for (int i = 0; i < numAttributes; i++) {
                    if (i != instance.classIndex() && instance.attribute(i).isNumeric()) {
                        attrs[i] += scales[i] * m_random.nextGaussian() * m_stdDev;
                    }
                }
                output.add(instance.copy(attrs));

                index = (index + 1) % instances.numInstances();
            }

            return output;
        } else {
            return instances;
        }
    }

    private double[] kthSmallestGapsOfInstances(Instances instances, int k) {
        if (instances.numInstances() < 2) {
            /* It makes no sense if there are few instances. */
            double[] result = new double[instances.numInstances()];
            Arrays.fill(result, 1);
            return result;
        }

        int numAttributes = instances.numAttributes();

        /* The 2-dimensional gaps ArrayList:
         * Outer axis along attributes, with fixed size.
         * Inner axis along (noncontinuous) instances, with variable size.
         */
        ArrayList<ArrayList<Double>> listGapsAttributes = new ArrayList<>();
        for (int i = 0; i < numAttributes; i++) {
            listGapsAttributes.add(new ArrayList<>());
        }

        double[] lastAttrs = instances.get(0).toDoubleArray();
        for (int i = 1; i < instances.numInstances(); i++) {
            double[] attrs = instances.get(i).toDoubleArray();
            for (int j = 0; j < numAttributes; j++) {
                double gap = Math.abs(attrs[j] - lastAttrs[j]);
                if (gap > Math.ulp(Math.max(attrs[j], lastAttrs[j]))) {
                    listGapsAttributes.get(j).add(gap);
                }
            }
            lastAttrs = attrs;
        }

        double[] result = new double[numAttributes];
        for (int i = 0; i < numAttributes; i++) {
            double[] gapsAttribute = Arrays.stream(listGapsAttributes.get(i).toArray(new Double[0]))
                    .mapToDouble(Double::doubleValue).toArray();
            int index = Math.min(k, gapsAttribute.length / 2);
            double gap;
            if (gapsAttribute.length != 0) {
                gap = Utils.kthSmallestValue(gapsAttribute, index);
            } else {
                gap = 1.0;
            }
            result[i] = gap;
        }

        return result;
    }

    @OptionMetadata(
            displayName = "Standard Deviation",
            description = "The standard deviation of the Gaussian noise to add.",
            displayOrder = 1,
            commandLineParamName = "stdDev",
            commandLineParamSynopsis = "-stdDev"
    )

    public double getStdDev() {
        return m_stdDev;
    }

    public void setStdDev(double stdDev) {
        this.m_stdDev = stdDev;
    }

    @OptionMetadata(
            displayName = "Number of sample",
            description = "Determine how many samples to generate per instance.",
            displayOrder = 2,
            commandLineParamName = "numSamples",
            commandLineParamSynopsis = "-numSamples"
    )

    public int getNumSamples() {
        return m_numSamples;
    }

    public void setNumSamples(int numSamples) {
        this.m_numSamples = numSamples;
    }

}
