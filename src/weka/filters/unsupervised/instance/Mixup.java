package weka.filters.unsupervised.instance;

import org.apache.commons.math3.distribution.BetaDistribution;
import weka.clusterers.SimpleKMeans;
import weka.core.*;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class Mixup extends InjectingFilter implements Randomizable {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(Mixup.class.getName());
    /**
     * The seed used in randomization
     */
    protected int m_seed = 0;

    /**
     * The parameter alpha of the Beta distribution.
     */
    protected double m_alpha = 0.05;

    /**
     * Determine how many samples to generate per instance.
     */
    protected int m_numSamples = 2;

    /**
     * An experimental option that defines the class of the mixed instance 0.
     */
    protected int m_mixedClass = 0;

    /**
     * An experimental option that applies a clustering step.
     */
    protected boolean m_preCluster = false;

    /**
     * Number of random generators.
     * For external function calls in which it cannot be assumed whether
     * the random sequence is to be altered (other than sequentially iterating)
     * or not, a standalone generator is assigned.
     */
    protected final int NUM_PRNG = 2;
    private final int PRNG_BETA_UNI = 1;
    private final int PRNG_SHUFFLE = 0;

    /**
     * Random generators for different random processes.
     */
    protected List<Random> m_PRNGs;

    /**
     * The Beta-distributed random generator.
     */
    BetaGenerator m_betaGenerator;

    protected interface BetaGenerator {
        double nextBeta();
    }

    class BetaGeneratorMath3 implements BetaGenerator, Serializable {
        private static final long serialVersionUID = 1L;
        BetaDistribution betaDistribution;
        Random random;

        public BetaGeneratorMath3(Random random, double alpha, double beta) {
            this.betaDistribution = new BetaDistribution(alpha, beta);
            this.random = random;
        }

        @Override
        public double nextBeta() {
            double y = random.nextDouble();
            return betaDistribution.inverseCumulativeProbability(y);
        }
    }

    public Mixup() {
        super();
        initPRNGs();
    }

    @Override
    public void setSeed(int seed) {
        this.m_seed = seed;
        initPRNGs();
    }

    @Override
    public int getSeed() {
        return m_seed;
    }

    @Override
    public String globalInfo() {
        return "Replace the training data with data generated using Mixup.";
    }


    @Override
    protected Instances process(Instances instances) throws Exception {
        if (!isFirstBatchDone()) {
            Instances output = getOutputFormat();
            int numOutputInstances = m_numSamples * instances.numInstances();

            /* Do not manipulate when there is zero or one instance, even such cases
             * rarely happen. */
            if (instances.numInstances() == 0) {
                return output;
            } else if (instances.numInstances() == 1) {
                for (int i = 0; i < numOutputInstances; i++) {
                    output.add(instances.get(0));
                }
                return output;
            }

            /* Shuffle the data */
            instances.randomize(m_PRNGs.get(PRNG_SHUFFLE));
            final int numClusters = instances.numClasses() * 2;
            int[] clusteredMap = sortByClusters(instances, numClusters);

            Instance pendingInstance = null;
            boolean havePendingInstance = false;
            int index = 0;
            initBeta();

            while (output.numInstances() < numOutputInstances) {
                if (havePendingInstance) {
                    output.add(pendingInstance);
                    havePendingInstance = false;
                    continue;
                }

                if ((index + 1) >= instances.numInstances()) {
                    index = 0;
                    /* Shuffle the data */
                    instances.randomize(m_PRNGs.get(PRNG_SHUFFLE));
                    clusteredMap = sortByClusters(instances, numClusters);
                }

                double lambda = m_betaGenerator.nextBeta();
                int firstIndex, secondIndex;
                if (m_preCluster) {
                    firstIndex = clusteredMap[index];
                    secondIndex = clusteredMap[index + 1];
                } else {
                    firstIndex = index;
                    secondIndex = index + 1;
                }
                Instance instance0 = comboOfInstances(new double[]{lambda, 1 - lambda}, m_mixedClass,
                        instances.get(firstIndex), instances.get(secondIndex));
                instance0.setWeight(lambda);
                output.add(instance0);

                Instance instance1 = instance0.copy(instance0.toDoubleArray());
                instance1.setClassValue(instances.get(secondIndex).classValue());
                instance1.setWeight(1 - lambda);
                pendingInstance = instance1;
                havePendingInstance = true;

                index += 2;
            }

            return output;
        } else {
            return instances;
        }
    }

    protected int[] sortByClusters(Instances instances, int numClusters) throws Exception {
        Instances strippedInstances = new Instances(instances);
        for (Instance instance : strippedInstances) {
            // Set the class value zero so that it will not affect the distance computation.
            instance.setClassValue(0);
        }
        // Pretend there is no class in the attributes, which is required by the clustering method.
        strippedInstances.setClassIndex(-1);

        ArrayList<ArrayList<Integer>> clusteredIndices = new ArrayList<>();
        for (int i = 0; i < numClusters; i++) {
            clusteredIndices.add(new ArrayList<>());
        }

        SimpleKMeans kMeans = new SimpleKMeans();
        try {
            kMeans.setNumClusters(numClusters);
            kMeans.buildClusterer(strippedInstances);

            for (int i = 0; i < strippedInstances.numInstances(); i++) {
                int clusterId = kMeans.clusterInstance(strippedInstances.get(i));
                clusteredIndices.get(clusterId).add(i);
            }

            return clusteredIndices.stream().flatMap(Collection::stream).mapToInt(i -> i).toArray();
        } catch (Exception e) {
            // Failed to cluster somehow.
            System.err.println("Failed to cluster instances.");
            e.printStackTrace();
        }

        // Return the array of identity mapping as fail-safe.
        return IntStream.range(0, instances.numInstances()).toArray();
    }

    private void initBeta() {
        m_betaGenerator = new BetaGeneratorMath3(m_PRNGs.get(PRNG_BETA_UNI), m_alpha, m_alpha);
    }

    private void initPRNGs() {
        if (m_PRNGs == null) {
            m_PRNGs = new ArrayList<>();
        } else {
            m_PRNGs.clear();
        }

        final int SEED_GAP = 1; // Whatever non-zero integer is fine.
        int seed = m_seed;
        for (int i = 0; i < NUM_PRNG; i++) {
            Random random = new Random(seed);
            seed += SEED_GAP;
            m_PRNGs.add(random);
        }

    }

    private Instance comboOfInstances(double[] weights, int classOf, Instance... instances) {
        if (weights.length != instances.length) {
            throw new IllegalArgumentException(String.format(
                    "Number of weights (%d) must be the same as that of instances (%d).",
                    weights.length, instances.length
            ));
        }

        if (weights.length == 0) {
            return null;
        }

        double[] attrs = new double[instances[0].numAttributes()];
        Arrays.fill(attrs, 0);

        for (int i = 0; i < weights.length; i++) {
            Instance instance = instances[i];
            if (instance.numAttributes() != attrs.length) {
                /* Combining instances with different numbers of attributes makes no sense. */
                throw new IllegalArgumentException(String.format(
                        "Instance %d has different number of attributes (%d) than expected (%d).",
                        i, instance.numAttributes(), attrs.length));
            }

            double[] instanceAttrs = instance.toDoubleArray();
            for (int j = 0; j < instance.numAttributes(); j++) {
                if (j == instance.classIndex()) {
                    /* For class attribute */
                    if (i == classOf) {
                        attrs[j] = instanceAttrs[j];
                    }
                } else if (instance.attribute(j).isNumeric()) {
                    /* For numeric attributes */
                    attrs[j] += weights[i] * instanceAttrs[j];
                } else {
                    /* For all other non-numeric and non-class attributes */
                    if (i == 0) {
                        /* Copy the information from the first instance */
                        attrs[j] = instanceAttrs[j];
                    } else {
                        LOGGER.warning(String.format(
                                "Cannot combine the attribute %d of type %s in instance %d.",
                                j, Attribute.typeToString(instance.attribute(j).type()), i
                        ));
                    }
                }
            }

        }

        return instances[0].copy(attrs);
    }

    @OptionMetadata(
            displayName = "Alpha",
            description = "The parameter alpha of the Beta distribution.",
            displayOrder = 1,
            commandLineParamName = "alpha",
            commandLineParamSynopsis = "-alpha")
    public double getAlpha() {
        return m_alpha;
    }
    public void setAlpha(double alpha) {
        this.m_alpha = alpha;
    }

    @OptionMetadata(
            displayName = "Number of sample",
            description = "Determine how many samples to generate per instance.",
            displayOrder = 2,
            commandLineParamName = "numSamples",
            commandLineParamSynopsis = "-numSamples"
    )
    public int getNumSamples() {
        return m_numSamples;
    }
    public void setNumSamples(int numSamples) {
        this.m_numSamples = numSamples;
    }

    @OptionMetadata(
            displayName = "Class to pick for the mixed instance",
            description = "An experimental option that defines the class of the mixed instance 0.",
            displayOrder = 3,
            commandLineParamName = "mixedClass",
            commandLineParamSynopsis = "-mixedClass"
    )
    public int getMixedClass() {
        return m_mixedClass;
    }
    public void setMixedClass(int mixedClass) {
        this.m_mixedClass = mixedClass;
    }

    @OptionMetadata(
            displayName = "Cluster before mixup",
            description = "An additional step before mixing up.",
            displayOrder = 4,
            commandLineParamIsFlag = true,
            commandLineParamName = "preCluster",
            commandLineParamSynopsis = "-preCluster"
    )
    public boolean getPreCluster() {
        return m_preCluster;
    }
    public void setPreCluster(boolean preCluster) {
        this.m_preCluster = preCluster;
    }
}
